import sys

if len(sys.argv) <= 1:
    print("Wait a word to play", file=sys.stderr)
    sys.exit(2)

def init_game(word):
    return tuple(["_" * len(word), word, 0])
    

def word_is_found(word, current):
    if len(word) != len(current):
        return False
    for index in range(len(word)):
        if word[index] != current[index]:
            return False
    return True

def end_game(word, current, nb_tries):
    if nb_tries >= 5:
        return 2
    if word_is_found(word, current):
        return 1
    return 0

def is_in_string(word, guess):
    for index in range(len(word)):
        if word[index] == guess:
            return True
    return False

def replace_in_string(word, current, guess):
    new_current = []
    for index in range(len(word)):
        if word[index] == guess:
            new_current.append(guess)
        elif current[index] != "_":
            new_current.append(current[index])
        else:
            new_current.append("_")
    return "".join(new_current)
    

current, word, nb_tries = init_game(sys.argv[1])

while not end_game(word, current, nb_tries):
    print("Current: {}".format(current))
    guess = input("Try a guess: ")
    if is_in_string(word, guess):
        current = replace_in_string(word, current, guess)
    else:
        nb_tries += 1

if word_is_found(word, current):
    print(current)
    print("You won!")
    sys.exit(0)

print("You lose")
sys.exit(1)
